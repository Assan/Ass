var mysql      = require('mysql');
//创建连接，使数据库和后台连接起来
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'ajaxdemo'
});
//连接数据库
connection.connect();
//后台发送命令到数据库，对数据库执行操作
connection.query('SELECT * FROM admin', function (error, results, fields) {
    console.log(results);
});

connection.end();